mongoose = require('mongoose');

// Définition du schéma pour les produits
const productSchema = new mongoose.Schema({
    title: String,
    description: String,
    imageUrl: String,
});

module.exports = mongoose.model('Product', productSchema);