const Product = require('../models/Produit');

exports.getOneThing = (req, res, next) => {
    Product.findOne({
        _id: req.params.id
    }).then(
        (product) => {
            res.status(200).json(product);
        }
    ).catch(
        (error) => {
            res.status(404).json({
                error: error
            });
        }
    );
};

exports.getAllStuff = (req, res, next) => {
    Product.find().then(
        (products) => {
            res.status(200).json(products);
        }
    ).catch(
        (error) => {
            res.status(400).json({
                error: error
            });
        }
    );
};