# Utilise l'image Node.js 14
FROM node:16

# Définit le répertoire de travail à /Mcommerce
WORKDIR /Mcommerce/produit

# Copie les fichiers du microservice dans le conteneur à /Mcommerce/produit
COPY . /Mcommerce/produit

# Installe les dépendances du projet
RUN npm install

# Expose le port 3005 du conteneur
EXPOSE 3005

# Commande à exécuter lors du démarrage du conteneur
CMD ["node", "serverProduit.js"]
